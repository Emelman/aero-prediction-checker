package main

const (
	aeroSpikeNamespace  = "main"
	aeroSpikeSet        = "profiles"
	aeroSpikeSubsBin    = "subscriptions"
	aeroSpikePredictBin = "predictions"

	predictionsBinLength = 24
)
