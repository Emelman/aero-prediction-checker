package main

import (
	"fmt"
	"sync"
	"sync/atomic"

	as "github.com/aerospike/aerospike-client-go"
	"github.com/golang/glog"

	g "xr/xutor/global"
	"xr/xutor/utils"
)

// PredictionsManager struct.
type PredictionsManager struct {
	client *as.Client
	wg     *sync.WaitGroup
	sync.RWMutex

	limit        int64
	logBorder    int64
	threads      int
	uniqProfiles map[int64]struct{}

	recordsProcessed               int64
	profilesWithSubscriptionStatus int64
	validProfiles                  int64
	notValidProfiles               int64
	notUniqRecords                 int64

	predictionBinNil       int64
	predictionCastTroubles int64
	predictionWrongLength  int64
	predictionsZeroValues  int64
}

// newPredictionsManager create new manager.
func newPredictionsManager(client *as.Client, limit, messageInterval int64, threadNumber int) *PredictionsManager {
	return &PredictionsManager{
		client: client,
		wg:     &sync.WaitGroup{},

		limit:        limit,
		logBorder:    messageInterval,
		threads:      threadNumber,
		uniqProfiles: make(map[int64]struct{}),
	}
}

func (o *PredictionsManager) executeProfilePredictionsCounter() error {
	reqBins := []string{
		g.EmailKeyNamePtrn,
		g.SubscriptionsKeyNamePtrn,
		g.PredictionsKeyNamePtrn,
	}

	filter := as.NewContainsRangeFilter(
		g.SubscriptionsKeyNamePtrn,
		as.ICT_MAPVALUES,
		int64(g.SbscStSubscriberOptIn),
		int64(g.SbscStSubscriberDoubleOptIn),
	)

	statement := as.NewStatement(aeroSpikeNamespace, aeroSpikeSet, reqBins...)

	err := statement.SetFilter(filter)
	if err != nil {
		glog.Errorf("aerospike set filter error: %v", err)
		return err
	}

	scanData, err := o.client.Query(nil, statement)
	if err != nil {
		glog.Errorf("reply Aerospike not OK. error: %v", err)
		return err
	}

	for i := 0; i < o.threads; i++ {
		o.wg.Add(1)

		go o.processResults(scanData.Results())
	}

	o.wg.Wait()

	return nil
}

func (o *PredictionsManager) processResults(results <-chan *as.Result) {
	defer o.wg.Done()

	for {
		request, ok := <-results
		if !ok {
			break
		}

		if !o.checkRecordIsUniq(request.Record) {
			atomic.AddInt64(&o.notUniqRecords, 1)
			continue
		}

		if o.checkLimitIsReached() {
			break
		}

		if request.Record == nil {
			atomic.AddInt64(&o.recordsProcessed, 1)
			o.processLogMessage()

			continue
		}

		if !o.checkSubscriptionStatusOk(request.Record) {
			atomic.AddInt64(&o.recordsProcessed, 1)
			o.processLogMessage()

			continue
		}

		if !o.checkPredictionIsValid(request.Record) {
			atomic.AddInt64(&o.recordsProcessed, 1)
			atomic.AddInt64(&o.notValidProfiles, 1)
			o.processLogMessage()

			continue
		}

		atomic.AddInt64(&o.recordsProcessed, 1)
		o.processLogMessage()
	}
}

// checkLimitIsReached check limit for amount records to process.
func (o *PredictionsManager) checkLimitIsReached() bool {
	if o.limit > 0 && atomic.LoadInt64(&o.recordsProcessed) >= o.limit {
		glog.Infof("limit reached: %v \n", o.limit)
		return true
	}

	return false
}

func (o *PredictionsManager) checkRecordIsUniq(record *as.Record) bool {
	pk, err := getPK(record.Bins[g.EmailKeyNamePtrn])
	if err != nil {
		glog.Errorf("cant get pk from email. error: %v", err)
		return false
	}

	o.RLock()
	_, ok := o.uniqProfiles[pk]
	o.RUnlock()

	if ok {
		return false
	}

	o.Lock()
	o.uniqProfiles[pk] = struct{}{}
	o.Unlock()

	return true
}

func getPK(val interface{}) (int64, error) {
	email, ok := val.(string)
	if !ok {
		return 0, fmt.Errorf("can't get email from value: %v", val)
	}

	return utils.GetMurmur3Int64Pk(email), nil
}

func (o *PredictionsManager) checkSubscriptionStatusOk(record *as.Record) bool {
	bin, ok := record.Bins[aeroSpikeSubsBin]
	if !ok {
		glog.Infoln("record bin is nil")
		return false
	}

	subscriptions, ok := bin.(map[interface{}]interface{})
	if !ok {
		glog.Infoln("can not cast subscriptions record to map")
		return false
	}

	for _, subscriptionStatus := range subscriptions {
		status, ok := subscriptionStatus.(int)
		if !ok {
			glog.Infof("can not get profile status on site. status: %v", subscriptionStatus)
			continue
		}

		if g.SubscriptionType(status) == g.SbscStUnsubscriber ||
			g.SubscriptionType(status) == g.SbscStComplainer {
			return false
		}
	}

	atomic.AddInt64(&o.profilesWithSubscriptionStatus, 1)

	return true
}

func (o *PredictionsManager) checkPredictionIsValid(record *as.Record) bool {
	bin, ok := record.Bins[aeroSpikePredictBin]
	if !ok {
		atomic.AddInt64(&o.predictionBinNil, 1)
		return false
	}

	predictions, ok := bin.([]interface{})
	if !ok {
		atomic.AddInt64(&o.predictionCastTroubles, 1)
		return false
	}

	if len(predictions) != predictionsBinLength {
		atomic.AddInt64(&o.predictionWrongLength, 1)
		return false
	}

	for _, value := range predictions {
		casted, ok := value.(float64)
		if !ok {
			continue
		}

		if casted != 0 {
			atomic.AddInt64(&o.validProfiles, 1)
			return true
		}
	}

	atomic.AddInt64(&o.predictionsZeroValues, 1)
	// all values are zero - not valid prediction
	return false
}

func (o *PredictionsManager) processLogMessage() {
	if atomic.LoadInt64(&o.recordsProcessed)%o.logBorder == 0 {
		glog.Infof("aerospike records processed: %d\n", atomic.LoadInt64(&o.recordsProcessed))
	}
}

func (o *PredictionsManager) logWorkResults() {
	glog.Infof(fmt.Sprintf(`Process finished: 
		profiles with valid predictions: %v,
		profiles with not valid predictions: %v,
		profiles with subscription status 11/12: %v.
		prediction bin is nil: %v,
		prediction cast troubles: %v,
		prediction wrong length: %v,
		prediction zero values: %v.
		Records dubles: %v.
		Records processed: %v.`,
		o.validProfiles,
		o.notValidProfiles,
		o.profilesWithSubscriptionStatus,
		o.predictionBinNil,
		o.predictionCastTroubles,
		o.predictionWrongLength,
		o.predictionsZeroValues,
		o.notUniqRecords,
		o.recordsProcessed))
}
