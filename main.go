package main

import (
	"flag"
	"runtime/debug"
	"time"

	as "github.com/aerospike/aerospike-client-go"
	"github.com/golang/glog"

	"xr/xutor/errh"
)

func main() {
	var (
		limit         int64
		threadsNumber int
		aeroHost      string
		aeroPort      int
		messageLog    int64
	)

	flag.Int64Var(&limit, "l", 0, "Records limit to process")
	flag.IntVar(&threadsNumber, "threads", 1, "Amount of threads script will process data from ssdb")
	flag.StringVar(&aeroHost, "aeroHost", "127.0.0.1", "Set aerospike host")
	flag.IntVar(&aeroPort, "aeroPort", 3000, "Set aerospike port")
	flag.Int64Var(&messageLog, "log", 1000, "Set amount of processed records before logging")

	defer func() {
		if r := recover(); r != nil {
			glog.Errorln(errh.PanicErrStr(r, debug.Stack(), "Main"))
		}

		glog.Flush()
	}()

	flag.Parse()

	// Get current time
	startTime := time.Now()

	// Init aerospike client
	asClient, err := as.NewClient(aeroHost, aeroPort)
	if err != nil {
		glog.Fatalf(
			"[%s] NewClient(%s,%d). Error connect to aerospike. Error: %s\n",
			errh.MARKER_INIT_FAIL,
			aeroHost,
			aeroPort,
			err,
		)
	}
	defer asClient.Close()

	predictAggregator := newPredictionsManager(asClient, limit, messageLog, threadsNumber)

	err = predictAggregator.executeProfilePredictionsCounter()
	if err != nil {
		glog.Errorf("process profiles expiration clean error: %v", err)
	}

	predictAggregator.logWorkResults()

	glog.Infof(`script work finished
		script start time: %v,
		script  end  time: %v,
		time elapsed: %v`,
		startTime.Format("2006-01-02 15:04:05"),
		time.Now().Format("2006-01-02 15:04:05"),
		time.Since(startTime))
}
